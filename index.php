<?php require_once 'php/SQL.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Comment Box</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="js/comments.js"></script>
    </head>
    <body>
        <div class="container">
            <h1>Write a comment</h1>
            <hr>
            <!-- Comment box -->
            <div class="form-horizontal">
                <div class="row form-group">
                    <label class="col-lg-1 col-md-1 col-sm-2 control-label">Name*</label>
                    <div class="col-lg-5 col-md-5 col-sm-4">
                        <input id="comment_name" class="form-control" type="text">
                    </div>
                    <label class="col-lg-1 col-md-1 col-sm-2 control-label">Email*</label>
                    <div class="col-lg-5 col-md-5 col-sm-4">
                        <input id="comment_email" class="form-control" type="email">
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-lg-1 col-md-1 col-sm-2 control-label">Comment*</label>
                    <div class="col-lg-11 col-md-11 col-sm-10">
                        <textarea id="comment_message" class="form-control"></textarea>
                    </div>
                </div>
                <button id="newCommentButton" class="col-lg-offset-1 col-md-offset-1 col-sm-offset-2 btn" type="button" onclick="addComment()">Submit</button>
                <span id="comment_insert_response" class="text-danger"></span>
                <hr>
            </div>
            <!-- Comment list -->
            <?php require_once 'php/comments/print.php'; ?>
        </div>
    </body>
</html>