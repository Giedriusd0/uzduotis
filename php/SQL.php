<?php
class SQL {
    // -- Params
    static $host = 'localhost';
    static $database = 'skight_mydb';
    static $username = 'user1';
    static $password = 'user1';

    // -- Execution
    private static function execute(string $query){
        $connection = new mysqli(self::$host, self::$username, self::$password, self::$database);
        if($connection->error){
            die();
        } else {
            $result = $connection->query($query);
            if($connection->error != null){
                die();
            }
        }
        $connection->close();
        return $result;
    }
    // -- Insert
    static function insert_comment(string $name, string $email, string $message){
        $date = date("Y-m-d H:i:s");
        return mysqli_fetch_assoc(self::execute("CALL insert_comment('$name', '$email', '$message', '$date');"));
    }
    static function insert_reply(int $commentId, string $name, string $email, string $message){
        $date = date("Y-m-d H:i:s");
        self::execute("CALL insert_reply('$commentId', '$name', '$email', '$message', '$date');");
    }
    // -- Select
    static function get_all_comments(){
        return self::execute("CALL get_comments()");
    }
    static function get_all_replies(){
        return self::execute("CALL get_replies()");
    }
    static function get_comments_count(){
        return mysqli_fetch_assoc(self::execute("CALL get_comments_count()"));
    }
}