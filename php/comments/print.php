<?php
    // Get all comments and replies
    $commentList = SQL::get_all_comments();
    $replyList = SQL::get_all_replies();
    // Count
    $totalCount = SQL::get_comments_count();

    // Comment count
    echo "<h2 id='comment_count'>{$totalCount['count']} comments</h2>";
    // Panels
    echo "<div id='comment_list' class='panel-group'>";

    foreach($commentList as $comment){
        print_comment($comment);
        foreach($replyList as $reply){
            if($reply['comment_id_fk'] === $comment['comment_id'])
                print_reply($reply);
        }
    }
    echo "</div>";

function print_comment(array $comment){
    $date = date("d M Y", strtotime($comment['comment_date']));
    echo <<<HTML
    <div class="panel panel-custom">
        <div class="panel-heading">
            <strong>{$comment['comment_name']}</strong>
            <span class="text-muted">{$date}</span>
            <button type="button" class="pull-right btn btn-link btn-link-custom" onclick="showReply(this, {$comment['comment_id']})"><strong><span class="glyphicon glyphicon-repeat"></span> reply</strong></a>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="comment-post">
                <p>{$comment['comment_message']}</p>
            </div>
        </div>
    </div>
HTML;
}

function print_reply(array $reply){
    $date = date("d M Y", strtotime($reply['comment_reply_date']));
    echo <<<HTML
    <div class="panel col-md-offset-1 col-sm-offset-1 col-xs-offset-1 panel-custom">
        <div class="panel-heading">
            <strong>{$reply['comment_reply_name']}</strong>
            <span class="text-muted">{$date}</span>
        </div>
        <div class="panel-body">
            <div class="comment-post">
                <p>{$reply['comment_reply_message']}</p>
            </div>
        </div>
    </div>
HTML;
}
?>