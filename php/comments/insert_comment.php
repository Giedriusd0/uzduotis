<?php
    // SQL
    require_once '../SQL.php';

    // Check name
    if(!isset($_POST['name']) || trim($_POST['name']) === ''){
        $response = new stdClass();
        $response->type = 'name';
        $response->message = "Please type in your name.";
        echo json_encode($response);
        die();
    } elseif (strlen($_POST['name'] > 25)){
        $response = new stdClass();
        $response->type = 'name';
        $response->message = "Your name is too long. Maximum is 25 symbols.";
        echo json_encode($response);
        die();
    }
    // Check email
    if(!isset($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $response = new stdClass();
        $response->type = 'email';
        $response->message = "Please type in a valid email address.";
        echo json_encode($response);
        die();
    } elseif ($_POST['email'] > 50) {
        $response = new stdClass();
        $response->type = 'email';
        $response->message = "Email adress is too long. Maximum is 50 symbols.";
        echo json_encode($response);
        die();
    }
    // Check message
    if(!isset($_POST['message']) ||trim($_POST['message']) === ''){
        $response = new stdClass();
        $response->type = 'message';
        $response->message = "Please type in a message.";
        echo json_encode($response);
        die();
    } elseif(strlen($_POST['message']) > 250){
        $response = new stdClass();
        $response->type = 'message';
        $response->message = "Message is too long. Maximum is 250 symbols.";
        echo json_encode($response);
        die();
    }
    $date = date("d M Y");
    if(!isset($_POST['commentId'])){
        // If id is not set, its a regular comment
        $newId = (SQL::insert_comment($_POST['name'], $_POST['email'], $_POST['message']));
        $response = new stdClass();
        $response->type = 'success';
        $response->message = <<<HTML
                            <div class="panel panel-custom animated fadeInDown">
                                <div class="panel-heading">
                                    <strong>{$_POST['name']}</strong>
                                    <span class="text-muted">{$date}</span>
                                    <button type="button" class="pull-right btn btn-link btn-link-custom" onclick="showReply(this, {$newId['id']})"><strong><span class="glyphicon glyphicon-repeat"></span> reply</strong></a>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="panel-body">
                                    <div class="comment-post">
                                        <p>{$_POST['message']}</p>
                                    </div>
                                </div>
                            </div>
HTML;
        echo json_encode($response);
    } else {
        // Id is set, its a reply
        SQL::insert_reply($_POST['commentId'], $_POST['name'], $_POST['email'], $_POST['message']);
        $response = new stdClass();
        $response->type = 'success';
        $response->message = <<<HTML
                            <div class="panel col-md-offset-1 col-sm-offset-1 col-xs-offset-1 panel-custom animated fadeInDown">
                                <div class="panel-heading">
                                    <strong>{$_POST['name']}</strong>
                                    <span class="text-muted">{$date}</span>
                                </div>
                                <div class="panel-body">
                                    <div class="comment-post">
                                        <p>{$_POST['message']}</p>
                                    </div>
                                </div>
                            </div>
HTML;
        echo json_encode($response);
    }
?>