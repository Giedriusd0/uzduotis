DELIMITER $$
DROP PROCEDURE IF EXISTS insert_comment $$
CREATE PROCEDURE insert_comment (_name VARCHAR(25), _email VARCHAR(50), _message VARCHAR(250), _date DATETIME)
	BEGIN
		INSERT INTO comment (comment_name, comment_email, comment_message, comment_date) 
        VALUES (_name, _email, _message, _date);
        SELECT LAST_INSERT_ID() as id;
	END $$

DROP PROCEDURE IF EXISTS insert_reply $$
CREATE PROCEDURE insert_reply (_comment_id INT, _name VARCHAR(25), _email VARCHAR(50), _message VARCHAR(250), _date DATETIME)
	BEGIN
		INSERT INTO comment_reply (comment_id_fk, comment_reply_name, comment_reply_email, comment_reply_message, comment_reply_date) 
        VALUES (_comment_id, _name, _email, _message, _date);
	END $$

DROP PROCEDURE IF EXISTS get_comments $$
CREATE PROCEDURE get_comments ()
	BEGIN
		SELECT * FROM comment ORDER BY comment_date DESC;
	END $$

DROP PROCEDURE IF EXISTS get_replies $$
CREATE PROCEDURE get_replies ()
	BEGIN
		SELECT * FROM comment_reply ORDER BY comment_reply_date DESC;
	END $$
DROP PROCEDURE IF EXISTS get_comments_count $$
CREATE PROCEDURE get_comments_count ()
	BEGIN 
		SELECT (SELECT COUNT(*) FROM comment) + (SELECT COUNT(*) FROM comment_reply) AS count;
	END $$
DELIMITER ;