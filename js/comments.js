function addComment(){
    // Boxes
    const nameBox = document.getElementById("comment_name");
    const emailBox = document.getElementById("comment_email");
    const messageBox = document.getElementById("comment_message");
    const commentCountBox = document.getElementById("comment_count");
    const list = document.getElementById("comment_list");
    // Values
    const name = nameBox.value;
    const email = emailBox.value;
    const message = messageBox.value;
    const vars = "name=" + name + "&email=" + email + "&message=" + message;

    // Reset boxes
    nameBox.style.borderColor = '';
    emailBox.style.borderColor = '';
    messageBox.style.borderColor = '';

    // Loading response
    document.getElementById("comment_insert_response").innerHTML = '<i class="fa fa-spinner fa-spin fa-1x fa-fw margin-bottom text-info">';
    // Disable button
    document.getElementById("newCommentButton").disabled = "disabled";

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            // Response
            const response = JSON.parse(this.response);
            // Enable button
            document.getElementById("newCommentButton").disabled = "";
            // On success
            if(response.type == 'success'){
                // Add new comment
                list.insertAdjacentHTML('afterbegin', response.message);
                // Update comment count
                commentCountBox.innerHTML = (Number(commentCountBox.innerHTML.replace(' comments', '')) + 1) + " comments";
                // Reset response
                document.getElementById("comment_insert_response").innerHTML = '';
                // Reset boxes
                nameBox.value = '';
                emailBox.value = '';
                messageBox.value = '';
            } else {
                document.getElementById("comment_insert_response").innerHTML = response.message;
                switch(response.type){
                    case 'name': nameBox.style.borderColor = 'red'; break;
                    case 'email':  emailBox.style.borderColor = 'red'; break;
                    case 'message': messageBox.style.borderColor = 'red'; break;
                }
            }
        }
    }
    xhttp.open("POST", "php/comments/insert_comment.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(vars);
}
function addReply(comment, commentId){
    // Boxes
    const nameBox = document.getElementById("comment_reply_name" + commentId);
    const emailBox =  document.getElementById("comment_reply_email" + commentId);
    const messageBox = document.getElementById("comment_reply_message" + commentId);
    const commentCountBox = document.getElementById("comment_count");

    // Values
    const name = nameBox.value;
    const email = emailBox.value;
    const message = messageBox.value;
    const vars = "name=" + name + "&email=" + email + "&message=" + message + "&commentId=" + commentId;

    // Reset boxes
    nameBox.style.borderColor = '';
    emailBox.style.borderColor = '';
    messageBox.style.borderColor = '';

    // Loading Response
    document.getElementById("comment_reply_insert_response" + commentId).innerHTML = '<i class="fa fa-spinner fa-spin fa-1x fa-fw margin-bottom text-info">';

    // Disable button
    comment.disabled = "true";
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200){
            // Response
            const response = JSON.parse(this.response);
            comment.disabled = "";
            if(response.type == 'success'){
                ((comment.parentNode).parentNode).parentNode.insertAdjacentHTML('beforebegin', response.message);
                hideReply(comment);
                commentCountBox.innerHTML = (Number(commentCountBox.innerHTML.replace(' comments', '')) + 1) + " comments";
            } else {
                document.getElementById("comment_reply_insert_response" + commentId).innerHTML = response.message;
                switch(response.type){
                    case 'name': nameBox.style.borderColor = 'red'; break;
                    case 'email':  emailBox.style.borderColor = 'red'; break;
                    case 'message': messageBox.style.borderColor = 'red'; break;
                }
            }
        }
    }
    xhttp.open("POST", "php/comments/insert_comment.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(vars);

}
function showReply(element, commentId){
    if(document.getElementById("replyForm" + commentId) == null){
    const parrent = (element.parentNode).parentNode;
    const form = '\
                <div id="replyForm' + commentId +'" class="panel col-lg-offset-1 col-md-offset-1 col-sm-offset-2 col-xs-offset-1 animated fadeInRight"> \
                    <div class="panel-body">\
                        <div class="form-horizontal"> \
                            <div class="row form-group"> \
                                <label class="col-lg-1 col-md-1 col-sm-2 control-label">Name*</label> \
                                <div class="col-lg-5 col-md-5 col-sm-4"> \
                                    <input id="comment_reply_name'+ commentId +'" class="form-control" type="text"> \
                                </div> \
                                <label class="col-lg-1 col-md-1 col-sm-2 control-label">Email*</label> \
                                <div class="col-lg-5 col-md-5 col-sm-4"> \
                                    <input id="comment_reply_email' + commentId + '" class="form-control" type="email"> \
                                </div> \
                            </div> \
                            <div class="row form-group"> \
                                <label class="col-lg-1 col-md-1 col-sm-2 control-label">Comment*</label> \
                                <div class="col-lg-11 col-md-11 col-sm-10"> \
                                    <textarea id="comment_reply_message' + commentId + '" class="form-control"></textarea> \
                                </div> \
                            </div> \
                            <button class="col-lg-offset-1 col-md-offset-1 col-sm-offset-2 btn" type="button" onclick="addReply(this, ' + commentId + ')">Submit</button> \
                            <button class="btn" type="button" onclick="hideReply(this)">Cancel</button> \
                            <span id="comment_reply_insert_response' + commentId + '" class="text-danger"></span> \
                        </div> \
                    </div> \
                </div> \
    ';
    parrent.insertAdjacentHTML('afterend', form);
    }
}
function hideReply(element){
    const parrent = ((element.parentNode).parentNode).parentNode;
    parrent.remove();
}